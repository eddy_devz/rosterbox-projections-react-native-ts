/**
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import { List } from 'react-native-paper';
import { useQuery, gql } from '@apollo/client'

declare const global: {HermesInternal: null | {}};

interface Players {
  position: string;
  team: string;
  player: string;
  projected_points: string;
}

interface PlayerProjectionData {
  players: Players[];
}

const GET_PLAYER_PROJECTIONS = gql`
  query GetPlayerProjections {
    players {
      player
      position
      team
      projected_points
    }
  }
`

const App = () => {
  const { loading, data } = useQuery<PlayerProjectionData>(
    GET_PLAYER_PROJECTIONS
  )
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
          {/* <Header /> */}
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>RosterBox</Text>
              <Text style={styles.sectionDescription}>
                2020 Preseason Projections for the Top 100+ Players
              </Text>
              {loading ? (
                <Text>Loading...</Text>
              ) : (
                <View>
                  {data && data.players.map(p => (
                    <List.Item key={p.position}
                    title={p.player}
                    description={p.projected_points}
                  />
                  ))}
                </View>
              )}
            </View>
            {/* <Button icon={require('../assets/chameleon.jpg')}>
                    Press me
            </Button> */}
          </View>
       
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
