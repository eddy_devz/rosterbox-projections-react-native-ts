/**
 * @format
 */

import * as React from 'react';
import { AppRegistry } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import { name as appName } from './app.json';
import App from './App';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql',
  cache: new InMemoryCache()
});

export default function Main() {
  return (
    <ApolloProvider client={client}>
    <PaperProvider>
      <App />
    </PaperProvider>
    </ApolloProvider>
  );
}

AppRegistry.registerComponent(appName, () => Main);
